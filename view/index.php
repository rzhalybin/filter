<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
	<head>
		<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
	</head>
	<body>
		<div class="filter">
			<form id="filter">
				<select name="source">
					<option selected></option>
					<option value="php">php</option>
					<option value="json">json</option>
					<option value="xml">xml</option>
				</select>
				<br>
				<select name="group">
					<option value="europe">europe</option>
					<option value="world">world</option>
					<option value="all" selected>all</option>
				</select>
					<br>
				<select name="field">
					<option></option>
					<option value="code">code</option>
					<option value="price">price</option>
					<option value="name">name</option>
				</select>
				<select name="type">
					<option></option>
					<option value="greater">></option>
					<option value="less"><</option>
					<option value="like">ilike</option>
				</select>
				<input type="text" name="value" value="">
				<br>
				<select name="sort">
					<option selected>unsorted</option>
					<option value="code">code</option>
					<option value="price">price</option>
					<option value="name">name</option>
				</select>
				<select name="order">
					<option selected></option>
					<option value="asc">asc</option>
					<option value="desc">desc</option>
				</select>
				<br>
				<input type="button" value="Filter">
				<input type="hidden" name="action" value="filter">
				<input type="hidden" name="ajax" value="1">
			</form>
		</div>
		<table>
			<thead>
				<td>group</td>
				<td>code</td>
				<td>price</td>
				<td>name</td>
			</thead>
			<tbody></tbody>
		</table>

		<script type="text/javascript">
			$(document).ready(function(){
				$('input[type="button"]').click(function(){
					var params = $('form').serialize();
					$.ajax({
						url: "/",
						data: params,
						dataType: 'json',
						success: function( data ) {
							if (data.success) {
								$('table tbody').html(data.html);
							}
							else {
								alert(data.message);
							}
						}
					});
				});
			});
		</script>
	</body>
</html>