
<div class="filter">
	<select name="source">
		<option selected></option>
		<option value="php" selected>php</option>
		<option value="json">json</option>
		<option value="xml">xml</option>
	</select>
	<select name="group">
		<option value="europe">europe</option>
		<option value="world">world</option>
		<option value="all" selected>all</option>
	</select>
	<select name="field">
		<option></option>
		<option value="code">code</option>
		<option value="price">price</option>
		<option value="name">name</option>
	</select>
	<select name="type">
		<option></option>
		<option value="greater">></option>
		<option value="less"><</option>
		<option value="like">ilike</option>
	</select>
	<input type="text" name="value" value="">
	<select name="sort">
		<option selected>unsorted</option>
		<option value="code">code</option>
		<option value="price">price</option>
		<option value="name">name</option>
	</select>
	<select name="order">
		<option selected></option>
		<option value="asc">asc</option>
		<option value="desc">desc</option>
	</select>
</div>
<table>
	<thead>
		<td>group</td>
		<td>code</td>
		<td>price</td>
		<td>name</td>
	</thead>
	<tbody>
		<?php
			/*foreach ($data as $row) {
				echo '<tr><td>'.$row['group'].'</td><td>'.$row['code'].'</td><td>'.$row['price'].'</td><td>'.$row['name'].'</td></tr>';
			}*/
		?>
	</tbody>
</table>
