<?php

class DecoderJSON extends Decoder 
{
	function decode() 
	{
		$result = array();
		$data = json_decode(file_get_contents($this->file));
		foreach ($data as $row) {
			$result[] = array(
				'group' => $row[3],
				'code'  => $row[0],
				'name'  => $row[1],
				'price' => $row[2],
			);
		}
		return $result;
	}
}