<?php

class Controller
{
	private $context;
	
	public function __construct() 
	{
		$this->context = new CommandContext();
	}
	
	public function getContext()
	{
		return $this->context;
	}
	
	public function process()
	{
		if (!$action = $this->context->get('action')) {
			$action = 'index';
		}
		$cmd = CommandFactory::getCommand($action);
		if ($data = $cmd->execute($this->context)) {
			return $data;
		}
		else {
			throw new Exception('Bad request');
		}
	}
}

