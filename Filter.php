<?php

class Filter
{
	private $data;
	private $key;
	
	public function __construct($data)
	{
		$this->data = $data;
	}
	
	public function run(CommandContext $context)
	{
		$res = $this->data;
		$group = $context->get('group');
		$field = $context->get('field');
		$type  = $context->get('type');
		$value = $context->get('value');
		$sort  = $context->get('sort');
		$order = $context->get('order');
		if ($group || ($field && $type && $value) || ($sort && $order)) {
			foreach ($res as $key => $row) {
				if ($group != 'all' && !empty($group) && $row['group'] != $group) {
					unset($res[$key]);
				}
				if ($field && $type && $value) {
					switch ($type) {
						case 'greater':
							if ($row[$field] <= $value) {
								unset($res[$key]);
							}
							break;
						case 'less':
							if ($row[$field] >= $value) {
								unset($res[$key]);
							}
							break;
						case 'like':
							if (stripos($row[$field], $value) === false) {
								unset($res[$key]);
							}
							break;
					}
				}
			}
			if ($sort && $order) {
				usort($res, msort($sort));
				if ($order == 'desc') {
					krsort($res);
				}
			}
		}
		return $res;
	}
}

function msort($key)
{
	return function ($a, $b) use ($key) {
						return strnatcmp($a[$key], $b[$key]);
					};
}