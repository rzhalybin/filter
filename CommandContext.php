<?php

class CommandContext
{
	private $params = array();
	private $error  = '';
	
	public function __construct() 
	{
		$this->params = $_REQUEST;
	}
	
	public function addParam($key, $value)
	{
		$this->params[$key] = $value;
	}
	
	public function get($key)
	{
		return isset($this->params[$key]) ? $this->params[$key] : null;
	}
	
	public function setError($error)
	{
		$this->error = $error;
	}
	
	public function getError()
	{
		return $this->error;
	}
}
