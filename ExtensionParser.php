<?php

class ExtensionParser extends Parser 
{
	protected $filename;
	
	public function make()
	{
		$ext = pathinfo($this->filename, PATHINFO_EXTENSION);
		$class = 'Decoder'.  strtoupper($ext);
		return new $class($this->filename);
	}
}

