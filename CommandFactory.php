<?php

class CommandFactory
{
	private static $dir = 'commands';
	
	public static function getCommand($action = 'index')
	{
		if (preg_match('/\W/', $action)) {
			throw new Exception('Illegal action name');
		}
		$class = ucfirst(strtolower($action)).'Command';
		$class_file = self::$dir.DIRECTORY_SEPARATOR.$class.'.php';
		
		if (!is_file($class_file)) {
			throw new Exception('Illegal action name');
		}
		require_once $class_file;
		$cmd = new $class();
		return $cmd;
	}
	
}

