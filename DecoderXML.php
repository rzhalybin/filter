<?php

class DecoderXML extends Decoder 
{
	function decode() 
	{
		$result = array();
		
		$map = array(
			'CODE'        => 'code',
			'VALUE'       => 'price',
			'DESCRIPTION' => 'name',
		);
		
		$data = file_get_contents($this->file);
		
		$p = xml_parser_create();
		xml_parse_into_struct($p, $data, $vals, $index);
		xml_parser_free($p);
		
		$i = 0; 
		while ($i < count($vals)) {
			$row = array();
			if ($vals[$i]['type'] == 'open' && isset($vals[$i]['attributes']['TYPE'])) {
				$row['group'] = $vals[$i]['attributes']['TYPE'];
				while ($vals[$i]['type'] != 'close' && $i < count($vals)) {
					if (isset($map[$vals[$i]['tag']])) {
						$row[$map[$vals[$i]['tag']]] = $vals[$i]['value'];
					}
					$i++;
				}
				$result[] = $row;
			}
			$i++;
		}

		return $result;
	}
}