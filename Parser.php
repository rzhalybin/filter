<?php

abstract class Parser 
{
	protected $filename;
	
	public function __construct($filename)
	{
		$this->filename = $filename;
	}

	public abstract function make();
}

