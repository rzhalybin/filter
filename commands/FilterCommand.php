<?php

class FilterCommand extends Command
{
	private $source_file = 'source/test.';
	
	public function execute(CommandContext $context)
	{
		$error = '';
		$response = array(
			'success' => 0,
			'message' => '',
			'html'    => '',
		);
		if (!$context->get('source')) {
			$error = 'Please select source.';
		}
		$new_file = $this->source_file.$context->get('source');
		if (!$error && !is_file($new_file)) {
			$error = 'System error.';
		}
		if (!$error) {
			$parser = new ExtensionParser($new_file);
			$data = $parser->make()->decode();
			$filter = new Filter($data);
			$data = $filter->run($context);
			$response['html'] = $this->view(array('data' => $data));
			$response['success'] = 1;
		}
		else {
			$response['message'] = $error;
		}
		return $response;
	}
}

