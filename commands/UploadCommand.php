<?php

class UploadCommand extends Command
{
	private $tmp_dir = 'tmp';
	
	public function execute(CommandContext $context)
	{
		if (isset($_FILES['file'])) {
			$ext = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
			$new_file = $this->tmp_dir.DIRECTORY_SEPARATOR.time().'.'.$ext;
			if (move_uploaded_file($_FILES['file']['tmp_name'], $new_file)) {
				$parser = new ExtensionParser($new_file);
				$data = $parser->make()->decode();
			}
		}
		return $this->view(array('data' => $data));
	}
}

