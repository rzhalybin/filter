<?php

class DecoderPHP extends Decoder 
{
	function decode() 
	{
		$result = array();
		$data = include $this->file;
		foreach ($data as $group => $elem) {
			foreach ($elem as $code => $info) {
				$result[] = array(
					'group' => $group,
					'code'  => $code,
					'name'  => $info['name'],
					'price' => $info['value'],
				);
			}
		}
		return $result;
	}
}