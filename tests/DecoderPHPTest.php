<?php

include_once 'Decoder.php';
include_once 'DecoderPHP.php';

class DecoderPHPTest extends PHPUnit_Framework_TestCase 
{
	function testDecode() 
	{
		$result = array(
					array(
						'group' => 'europe',
						'code'  => 'PLN',
						'name'  => 'zloty polski',
						'price' => 1.0,
					),
					array(
						'group' => 'europe',
						'code'  => 'EUR',
						'name'  => 'euro',
						'price' => 4.15,
					),
					array(
						'group' => 'europe',
						'code'  => 'CHF',
						'name'  => 'frank szwajcarski',
						'price' => 3.36,
					),
					array(
						'group' => 'world',
						'code'  => 'USD',
						'name'  => 'dolar amerykanski',
						'price' => 3.43,
					),
					array(
						'group' => 'world',
						'code'  => 'PHP',
						'name'  => 'peso filipinskie',
						'price' => 0.067,
					),
					array(
						'group' => 'world',
						'code'  => 'RUB',
						'name'  => 'rubel rosyjski',
						'price' => 0.091,
					),
					array(
						'group' => 'world',
						'code'  => 'INR',
						'name'  => 'rupia indyjska',
						'price' => 4.94,
					),
				);
		$decoder = new DecoderPHP('source/test.php');
		$this->assertSame($result, $decoder->decode());
	}
}

