<?php
include_once 'CommandContext.php';

class CommandContextTest extends PHPUnit_Framework_TestCase
{
	public function testConstruct()
	{
		$_REQUEST['test'] = 'test';
		$context = new CommandContext();
		$this->assertEquals('test', $context->get('test'));
	}
	
	public function testAddParam()
	{
		$_REQUEST['test'] = 'test1';
		$context = new CommandContext();
		$context->addParam('test', 'test1');
		$this->assertEquals('test1', $context->get('test'));
		return $context;
	}
	
	/**
	 * @depends testAddParam
	 */
	public function testGet($context)
	{
		$this->assertEquals('test1', $context->get('test'));
	}
}

