<?php

include_once 'Command.php';
include_once 'CommandFactory.php';

class CommandFactoryTest extends PHPUnit_Framework_TestCase
{
	public function testGetCommand($action = 'index')
	{
		$command = CommandFactory::getCommand('index');
		$this->assertInstanceOf('IndexCommand', $command);
	}
}