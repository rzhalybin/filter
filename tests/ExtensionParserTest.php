<?php

include_once 'Parser.php';
include_once 'ExtensionParser.php';

class ExtensionParserTest extends PHPUnit_Framework_TestCase
{
	public function testMake()
	{
		$parser = new ExtensionParser('source/test.php');
		$this->assertInstanceOf('DecoderPHP', $parser->make());
	}
}

