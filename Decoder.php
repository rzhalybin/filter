<?php

abstract class Decoder 
{
	protected $file;
	
	function __construct($file)
	{
		$this->file = $file;
	}
	
	abstract function decode();
}

