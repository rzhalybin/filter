<?php

abstract class Command
{
	private $dir = 'view';
	
	abstract public function execute(CommandContext $context);
	
	public function view($params = array()) 
	{
		if ($params) {
			foreach ($params as $name => $value) {
				$$name = $value;
			}
		}
		$view_file = $this->dir.DIRECTORY_SEPARATOR.strtolower(str_replace('Command', '', get_class($this))).'.php';
		if (!is_file($view_file)) {
			throw new Exception('System error');
		}
		ob_start();
		include $view_file;
		$out = ob_get_contents();
		ob_end_clean();
		return $out;
	}
}
