<?php

spl_autoload_register();

$controller = new Controller();
$data = $controller->process();
if ($controller->getContext()->get('ajax')) {
	echo json_encode($data);
}
else {
	echo $data;
}